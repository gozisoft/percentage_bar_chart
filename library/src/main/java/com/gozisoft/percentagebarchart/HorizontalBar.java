package com.gozisoft.percentagebarchart;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.RestrictTo;

import java.security.InvalidParameterException;

import static androidx.annotation.RestrictTo.Scope.LIBRARY_GROUP;

/**
 * Created by rgozim on 05/01/2017.
 */
public class HorizontalBar extends View {

    public static final String TAG = HorizontalBar.class.getSimpleName();

    private final TextPaint mTextPaint = new TextPaint();
    private final Rect mTextBounds = new Rect();

    private final Rect mDrawableBounds = new Rect();
    private final Rect mDrawablePadding = new Rect();

    private GradientDrawable mDrawable;

    private String mLabel;
    private float mTextX;
    private float mTextY;
    private int mGravity = Gravity.END | Gravity.CENTER_VERTICAL;

    private boolean mDisplayTextOutside = false;

    HorizontalBar(Context context, Entry entry) {
        super(context);
        mGravity = entry.gravity;
        mLabel = entry.label;
        mTextPaint.setColor(entry.textColor);
        mTextPaint.setTextSize(entry.textSize);
        if (!TextUtils.isEmpty(mLabel)) {
            mTextPaint.getTextBounds(mLabel, 0, mLabel.length(), mTextBounds);
        }

        Drawable drawable = getResources().getDrawable(entry.drawableRes);
        if (drawable != null) {
            if (!(drawable instanceof GradientDrawable)) {
                throw new IllegalStateException("Drawable must be of type GradientDrawable");
            } else {
                mDrawable = (GradientDrawable) drawable;

                // We don't want to change other resources
                mDrawable.mutate();

                // Get the padding of the drawable
                mDrawable.getPadding(mDrawablePadding);
            }
        }
    }

    public HorizontalBar(Context context) {
        this(context, null, 0);
    }

    public HorizontalBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HorizontalBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context, attrs, defStyleAttr, 0);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public HorizontalBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr,
                         int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.init(context, attrs, defStyleAttr, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Draw our shape
        mDrawable.draw(canvas);

        if (!TextUtils.isEmpty(mLabel)) {
            // Then text on top
            canvas.drawText(mLabel, mTextX, mTextY, mTextPaint);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int viewHeight = MeasureSpec.getSize(heightMeasureSpec);
        final int viewWidth = MeasureSpec.getSize(widthMeasureSpec);
        final int barPadding = mDrawablePadding.left + mDrawablePadding.right;

        // The width we have to draw in is the width of this view minus
        // any drawable padding horizontally.
        final int availableViewWidth = viewWidth - barPadding;

        // Our minimum width is the width of the text, plus any
        // horizontal view padding.
        final int minw = getSuggestedMinimumWidth();
        final int width;
        final int barWidth;
        if (availableViewWidth < minw) {
            // Set the view's width to the minimum width required to
            // display a minimum sized bar graph and some text to it's right
            barWidth = viewWidth; // getMinimumWidth() + barPadding;
            width = barWidth + minw;
            mDisplayTextOutside = true;
        } else {
            // The text fits fine within view width
            barWidth = viewWidth;
            width = viewWidth;
            mDisplayTextOutside = false;
        }

        // Set the size discounting padding
        mDrawable.setSize(barWidth, viewHeight);

        // Set the width and height of this view
        setMeasuredDimension(width, getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        // Calculate drawable bounds excluding any padding
        int drawLeft = mDrawablePadding.left;
        int drawTop = mDrawablePadding.top;
        int drawRight = drawLeft + (mDrawable.getIntrinsicWidth() - mDrawablePadding.right);
        int drawBottom = drawTop + (mDrawable.getIntrinsicHeight() - mDrawablePadding.bottom);

        // We have to set the bounds including any padding in order to have
        // the shape draw
        mDrawable.setBounds(drawLeft, drawTop, drawRight, drawBottom);
        mDrawableBounds.set(mDrawable.getBounds());

        // Determine text x and y coordinates.
        float x;
        if (!TextUtils.isEmpty(mLabel)) {
            if (mDisplayTextOutside) {
                // Try to fit text within allowable height.
                adjustTextHeight(h);

                // If the text is too long to display inside the drawable
                // we place it on the end of the bar.
                final int layoutDirection = getParent().getLayoutDirection();
                if (layoutDirection == LAYOUT_DIRECTION_LTR) {
                    // Text draws to the right of the bar
                    x = mDrawable.getIntrinsicWidth() + getPaddingLeft();
                } else {
                    // Text draws to the left of the bar
                    x = (w - mDrawable.getIntrinsicWidth()) -
                            (mTextPaint.measureText(mLabel) + getPaddingLeft());
                }
            } else {
                // These are the constraints that the text can sit inside
                final int textLeft = drawLeft + getPaddingLeft();
                final int textRight = drawRight - getPaddingRight();
                final int textTop = drawTop + getPaddingTop();
                final int textBottom = drawBottom - getPaddingBottom();
                final int actualHeight = textBottom - textTop;
                adjustTextHeight(actualHeight);

                // Calculate where to lie on the horizontal
                final int gravity = mGravity & Gravity.HORIZONTAL_GRAVITY_MASK;
                switch (gravity) {
                    case Gravity.LEFT:
                        x = textLeft;
                        break;
                    case Gravity.RIGHT:
                        float radialWidth = determineCornerRadialWidth(gravity);
                        x = (textRight - radialWidth) - mTextPaint.measureText(mLabel);
                        break;
                    default:
                        throw new RuntimeException("Unsupported gravity");
                }
            }

            // Calculate center y of view and offset vertically down using
            // half text height to render in exact center
            float y = mDrawableBounds.centerY() + (mTextBounds.height() >> 1);

            mTextX = x;
            mTextY = y;
        }
    }

    /**
     * The width of the bar is constrained to the width of the text
     *
     * @return
     */
    @Override
    protected int getSuggestedMinimumWidth() {
        if (!TextUtils.isEmpty(mLabel)) {
            // Text does not overlap the curved portion of the drawable. That is
            // effectively space the text cannot occupy. We have to add it
            // to our minimum width requirement for text
            int radialWidth = (int) determineCornerRadialWidth();

            // Space needed for text to draw - left & right padding & radial curve
            // at placement of text drawing.
            return (int) mTextPaint.measureText(mLabel) + getPaddingLeft() +
                    getPaddingRight() + radialWidth;
        }
        return super.getSuggestedMinimumWidth();
    }

    /**
     * The measured width of the bar chart minus any corner radius.
     * @return the width of the bar char
     */
    @RestrictTo(LIBRARY_GROUP)
    public int getMeasureBarWidth() {
        return mDrawable.getIntrinsicWidth() - (int) determineCornerRadialWidth();
    }

    /**
     * Sets the string value of the text value label.
     *
     * @param label the label to set
     * @attr ref R.styleable#HorizontalBar_text
     */
    public void setText(String label) {
        if (TextUtils.isEmpty(mLabel) || !mLabel.equals(label)) {
            mLabel = label;
            requestLayout();
        }
    }

    /**
     * Sets the text size in SP. If this size exceeds the height
     * of the view, a smaller size will be determined
     * during {@link #onMeasure(int, int)}
     *
     * @param size
     * @attr ref R.styleable#HorizontalBar_size
     */
    public void setTextSize(float size) {
        setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
    }

    /**
     * Set the default text size to a given unit and value. See {@link
     * TypedValue} for the possible dimension units.
     *
     * @param unit The desired dimension unit.
     * @param size The desired size in the given units.
     */
    public void setTextSize(int unit, float size) {
        Context c = getContext();
        Resources r;

        if (c == null)
            r = Resources.getSystem();
        else
            r = c.getResources();

        setRawTextSize(TypedValue.applyDimension(
                unit, size, r.getDisplayMetrics()));
    }

    /**
     * @param size the text size in pixels.
     */
    public void setRawTextSize(float size) {
        if (mTextPaint.getTextSize() != size) {
            mTextPaint.setTextSize(size);
            requestLayout();
        }
    }

    /**
     * @param color
     */
    public void setTextColor(@ColorInt int color) {
        if (mTextPaint.getColor() != color) {
            mTextPaint.setColor(color);
            invalidate();
        }
    }

    /**
     * @param colorRes
     */
    public void setTextColorRes(@ColorRes int colorRes) {
        int color = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            color = getResources().getColor(colorRes, null);
        } else {
            color = getResources().getColor(colorRes);
        }
        setTextColor(color);
    }

    public void setGravity(int gravity) {
        if (mGravity != gravity) {
            mGravity = gravity;
            invalidate();
        }
    }

    public void setPercentage(float percentage) {
        PercentageBarChartLayout.LayoutParams lp = (PercentageBarChartLayout.LayoutParams)
                getLayoutParams();
        if (lp == null) {
            throw new IllegalStateException("Horizontal bar should be attached to a parent");
        }

        if (lp.getPercentLayoutInfo().widthPercent != percentage) {
            lp.getPercentLayoutInfo().widthPercent = percentage;
            requestLayout();
        }
    }

    private void adjustTextHeight(int boundsHeight) {
        // If the text is a greater height than the view, then
        // loop until we can find a size that fits.
        while (true) {
            // Obtain the bounds of the text.
            mTextPaint.getTextBounds(mLabel, 0, mLabel.length(), mTextBounds);

            // final int textHeight = (int) getTextHeight(entry.mTextPaint);
            int textHeight = mTextBounds.height();
            if (textHeight < boundsHeight) {
                break;
            }

            // Shrink the height of the text until we can ge a height that
            // fits.
            float textSize = mTextPaint.getTextSize();
            mTextPaint.setTextSize(textSize - 0.9f);
        }
    }

    private float determineCornerRadialWidth() {
        final int gravity = mGravity & Gravity.HORIZONTAL_GRAVITY_MASK;
        return determineCornerRadialWidth(gravity);
    }

    private float determineCornerRadialWidth(int horizontalGravity) {
        float[] cornerRadii = getCornerRadii();
        if (cornerRadii != null) {
            if (horizontalGravity == Gravity.RIGHT) {
                return Math.max(cornerRadii[2], cornerRadii[4]);
            } else if (horizontalGravity == Gravity.LEFT) {
                return Math.max(cornerRadii[0], cornerRadii[6]);
            } else {
                throw new InvalidParameterException("horizontalGravity should compared against" +
                        "AND against Gravity.HORIZONTAL_GRAVITY_MASK");
            }
        } else {
            return getCornerRadius();
        }
    }

    private float determineCornerRadialHeight() {
        float[] cornerRadii = getCornerRadii();
        if (cornerRadii != null) {
            // We look at the y-dimension radius on the bottom and top
            float yTop = Math.max(cornerRadii[1], cornerRadii[3]);
            float yBottom = Math.max(cornerRadii[5], cornerRadii[7]);
            return yTop + yBottom;
        } else {
            return getCornerRadius();
        }
    }

    private float getCornerRadius() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return mDrawable.getCornerRadius();
        } else {
            try {
                return GradientDrawableCompat.getCornerRadius(mDrawable);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                Log.e(TAG, "Failed to get corner radius via reflection.", e);
            }
        }

        return -1;
    }

    private float[] getCornerRadii() {
        try {
            return GradientDrawableCompat.getCornerRadii(mDrawable);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            Log.e(TAG, "Failed to get cornerRadii via reflection.", e);
        }
        return null;

        // This version crashes when we use the Android N implementation due
        // to null exception.
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return mDrawable.getCornerRadii();
        }*/
    }

    private void init(Context context, @Nullable AttributeSet attrs, int defStyleAttr,
                      int defStyleRes) {
        TypedArray ta = null;
        try {
            ta = context.getTheme().obtainStyledAttributes(attrs, R.styleable.HorizontalBar,
                    defStyleAttr, defStyleRes);

            Drawable drawable = ta.getDrawable(R.styleable.HorizontalBar_shapeDrawable);
            if (drawable != null) {
                if (!(drawable instanceof GradientDrawable)) {
                    throw new IllegalStateException("Drawable must be of type GradientDrawable");
                } else {
                    mDrawable = (GradientDrawable) drawable;

                    // We don't want to change other resources
                    mDrawable.mutate();

                    // Get the padding of the drawable
                    mDrawable.getPadding(mDrawablePadding);
                }
            }

            // Set a gravity value
            mGravity = ta.getInt(R.styleable.HorizontalBar_gravity, Gravity.END);

            // Set the colour
            mTextPaint.setColor(ta.getColor(R.styleable.HorizontalBar_textColor, Color.WHITE));
            mTextPaint.setTextSize(ta.getDimension(R.styleable.HorizontalBar_textSize, 15));

            // Set the text label
            mLabel = ta.getString(R.styleable.HorizontalBar_text);
            if (!TextUtils.isEmpty(mLabel)) {
                // Obtain the bounds of the text.
                mTextPaint.getTextBounds(mLabel, 0, mLabel.length(), mTextBounds);
            }
        } finally {
            if (ta != null) {
                ta.recycle();
            }
        }
    }

    /**
     * Builder class for simplified creation and
     * instantiation of a {@link HorizontalBar}.
     */
    public static class Entry {
        String label;
        float percent;
        float textSize;
        int drawableRes;
        int textColor;
        int gravity = Gravity.END | Gravity.CENTER_VERTICAL;

        public Entry setPercent(float percentage) {
            this.percent = percentage;
            return this;
        }

        public Entry setDrawable(@DrawableRes int drawableRes) {
            this.drawableRes = drawableRes;
            return this;
        }

        public Entry setTextSize(float size) {
            return setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
        }

        public Entry setTextSize(int unit, float size) {
            return setRawTextSize(TypedValue.applyDimension(
                    unit, size, Resources.getSystem().getDisplayMetrics()));
        }

        public Entry setRawTextSize(float size) {
            this.textSize = size;
            return this;
        }

        public Entry setTextColor(@ColorInt int colour) {
            this.textColor = colour;
            return this;
        }

        public Entry setGravity(int gravity) {
            this.gravity = gravity;
            return this;
        }

        public Entry setLabel(String label) {
            this.label = label;
            return this;
        }

        HorizontalBar build(Context context) {
            return new HorizontalBar(context, this);
        }
    }

}
