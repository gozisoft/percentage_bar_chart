package com.gozisoft.percentagebarchart;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.core.view.ViewCompat;
import androidx.percentlayout.widget.PercentLayoutHelper;

/**
 * Created by rgozim on 07/01/2017.
 */

public class PercentageBarChartLayout extends ViewGroup {

    final PercentLayoutHelper mHelper = new PercentLayoutHelper(this);

    int mGravity = Gravity.START;
    int mTotalLength;

    public PercentageBarChartLayout(Context context) {
        this(context, null);
    }

    public PercentageBarChartLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PercentageBarChartLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr, 0);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PercentageBarChartLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if (!(child instanceof HorizontalBar)) {
            throw new IllegalArgumentException("Only HorizontalBar instances can be " +
                    "added to TabLayout");
        }
        super.addView(child, index, params);
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        LayoutParams lp = new LayoutParams(getContext(), attrs);
        lp.width = LayoutParams.WRAP_CONTENT;
        lp.height = LayoutParams.MATCH_PARENT;
        return lp;
    }

    @Override
    protected LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
    }

    @Override
    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
        LayoutParams lp = new LayoutParams(p);
        lp.width = LayoutParams.WRAP_CONTENT;
        lp.height = LayoutParams.MATCH_PARENT;
        return lp;
    }

    @Override
    protected boolean checkLayoutParams(ViewGroup.LayoutParams p) {
        return p instanceof LayoutParams;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        mHelper.adjustChildren(widthMeasureSpec, heightMeasureSpec);
        onMeasureInternal(widthMeasureSpec, heightMeasureSpec);
        if (mHelper.handleMeasuredStateTooSmall()) {
            onMeasureInternal(widthMeasureSpec, heightMeasureSpec);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        onLayoutInternal(changed, left, top, right, bottom);
        mHelper.restoreOriginalParams();
    }

    public HorizontalBar addEntry(HorizontalBar.Entry entry) {
        // Generate default layout params and set the width percent
        LayoutParams lp = generateDefaultLayoutParams();
        lp.getPercentLayoutInfo().widthPercent = entry.percent;

        // Create the bar view
        HorizontalBar bar = entry.build(getContext());
        // bar.setMinimumWidth(20);

        // Add it as child to this layout
        addView(bar, lp);

        // Return the bar, in case someone needs it for whatever
        return bar;
    }

    private void onMeasureInternal(int widthMeasureSpec, int heightMeasureSpec) {
        mTotalLength = 0;
        int maxHeight = 0;
        int childState = 0;

        final int count = getChildCount();
        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() == GONE) {
                continue;
            }

            // Measure the child.
            measureChildWithMargins(child, widthMeasureSpec, 0, heightMeasureSpec, 0);

            final MarginLayoutParams lp = (MarginLayoutParams) child.getLayoutParams();

            // Increment the total occupied width
            mTotalLength += Math.max(child.getMinimumWidth(),
                    child.getMeasuredWidth() + lp.leftMargin + lp.rightMargin);

            // Adjust the height based on the tallest child.
            maxHeight = Math.max(maxHeight,
                    child.getMeasuredHeight() + lp.topMargin + lp.bottomMargin);
            childState = combineMeasuredStates(childState, child.getMeasuredState());
        }

        setMeasuredDimension(getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec),
                getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec));

        // Check against our minimum height
        // maxHeight = Math.max(maxHeight, getSuggestedMinimumHeight());

        /*setMeasuredDimension(getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec),
                resolveSizeAndState(maxHeight, heightMeasureSpec, childState << MEASURED_HEIGHT_STATE_SHIFT));*/
    }

    private void onLayoutInternal(boolean changed, int left, int top, int right, int bottom) {
        // These are the far left and right edges in which we are performing layout.
        final int parentLeft = getPaddingLeft();
        final int parentRight = right - left - getPaddingRight();

        // These are the top and bottom edges in which we are performing layout.
        final int parentTop = getPaddingTop();
        final int parentBottom = bottom - top - getPaddingBottom();

        // Measure the width and height from the outer bounds
        final int width = parentRight - parentLeft;
        final int height = parentBottom - parentTop;

        final int gravity = Gravity.getAbsoluteGravity(
                mGravity & Gravity.HORIZONTAL_GRAVITY_MASK,
                ViewCompat.getLayoutDirection(this)
        );
        final int childCount = getChildCount();

        int lastX;
        switch (gravity) {
            case Gravity.LEFT:
                lastX = parentLeft;
                break;
            case Gravity.RIGHT:
                lastX = width - mTotalLength;
                break;
            default:
                throw new RuntimeException("Unsupported gravity");
        }

        // The number of children in this layout
        int dir = 1;
        int start = 0;
        if (ViewCompat.getLayoutDirection(this) ==
                ViewCompat.LAYOUT_DIRECTION_RTL) {
            dir = -1;
            start = childCount - 1;
        }

        for (int i = 0; i < getChildCount(); ++i) {
            final int childIndex = start + dir * i;
            HorizontalBar child = (HorizontalBar) getChildAt(childIndex);
            if (child.getVisibility() == GONE) {
                continue;
            }

            final int cw = child.getMeasureBarWidth();
            int nextX = lastX + cw;

            // Place the child.
            child.layout(lastX, parentTop, lastX + child.getMeasuredWidth(), parentBottom);

            // Keep track of the last x draw
            lastX = nextX;
        }
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        TypedArray ta = null;
        try {
            ta = context.getTheme().obtainStyledAttributes(attrs,
                    R.styleable.PercentageBarChartLayout,
                    defStyleAttr, defStyleRes);

            mGravity = ta.getInt(R.styleable.PercentageBarChartLayout_gravity,
                    Gravity.START);
        } finally {
            if (ta != null) {
                ta.recycle();
            }
        }
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams
            implements PercentLayoutHelper.PercentLayoutParams {

        private PercentLayoutHelper.PercentLayoutInfo mPercentLayoutInfo;
        private int mGravity = Gravity.START;

        public LayoutParams(Context c, AttributeSet attrs) {
            super(c, attrs);
            mPercentLayoutInfo = PercentLayoutHelper.getPercentLayoutInfo(c, attrs);
        }

        public LayoutParams(int width, int height) {
            super(width, height);
        }

        public LayoutParams(ViewGroup.LayoutParams source) {
            super(source);
        }

        @Override
        public PercentLayoutHelper.PercentLayoutInfo getPercentLayoutInfo() {
            if (mPercentLayoutInfo == null) {
                mPercentLayoutInfo = new PercentLayoutHelper.PercentLayoutInfo();
            }
            return mPercentLayoutInfo;
        }

        @Override
        protected void setBaseAttributes(TypedArray a, int widthAttr, int heightAttr) {
            PercentLayoutHelper.fetchWidthAndHeight(this, a, widthAttr, heightAttr);
        }
    }

}