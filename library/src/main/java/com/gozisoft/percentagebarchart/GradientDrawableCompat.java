package com.gozisoft.percentagebarchart;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;

import java.lang.reflect.Field;

/**
 * Created by Riad on 20/01/2017.
 */

public class GradientDrawableCompat {

    private static final String TAG = GradientDrawableCompat.class.getSimpleName();

    private static Field sGradientState;
    private static Field sGetCornerRadiusMethod;
    private static Field sGetCornerRadiiMethod;

    private static Object getGradientState(Drawable drawable) throws
            NoSuchFieldException,
            IllegalAccessException {
        if (sGradientState == null) {
            sGradientState = GradientDrawable.class
                    .getDeclaredField("mGradientState");
            sGradientState.setAccessible(true);
        }
        return sGradientState.get(drawable);
    }

    public static float getCornerRadius(Drawable drawable) throws NoSuchFieldException,
            IllegalAccessException {
        Object gradientState = getGradientState(drawable);
        if (sGetCornerRadiusMethod == null) {
            sGetCornerRadiusMethod = gradientState.getClass()
                    .getDeclaredField("mRadius");
            sGetCornerRadiusMethod.setAccessible(true);
        }
        return (float) sGetCornerRadiusMethod.get(gradientState);
    }

    public static float[] getCornerRadii(Drawable drawable) throws NoSuchFieldException,
            IllegalAccessException {
        Object gradientState = getGradientState(drawable);

        if (sGetCornerRadiiMethod == null) {
            sGetCornerRadiiMethod = gradientState.getClass()
                    .getDeclaredField("mRadiusArray");
            sGetCornerRadiiMethod.setAccessible(true);
        }
        return (float[]) sGetCornerRadiiMethod.get(gradientState);
    }
}
