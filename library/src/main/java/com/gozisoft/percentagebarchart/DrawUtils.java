package com.gozisoft.percentagebarchart;

import android.graphics.Paint;
import android.graphics.Path;
import android.text.TextPaint;

/**
 * Created by rgozim on 05/01/2017.
 */

public class DrawUtils {
    /**
     * Allows for drawing a rounded rect with specific corners
     * set to round.
     * <p>
     * From http://stackoverflow.com/a/35668889
     *
     * @param left
     * @param top
     * @param right
     * @param bottom
     * @param rx
     * @param ry
     * @param tl
     * @param tr
     * @param br
     * @param bl
     * @return
     */
    public static Path RoundedRect(float left, float top, float right, float bottom,
                                   float rx, float ry,
                                   boolean tl, boolean tr, boolean br, boolean bl) {
        Path path = new Path();
        if (rx < 0) rx = 0;
        if (ry < 0) ry = 0;
        float width = right - left;
        float height = bottom - top;
        if (rx > width / 2) rx = width / 2;
        if (ry > height / 2) ry = height / 2;
        float widthMinusCorners = widthMinusCorners(width, rx);
        float heightMinusCorners = heightMinusCorners(height, ry);

        path.moveTo(right, top + ry);
        if (tr)
            path.rQuadTo(0, -ry, -rx, -ry);//top-right corner
        else {
            path.rLineTo(0, -ry);
            path.rLineTo(-rx, 0);
        }
        path.rLineTo(-widthMinusCorners, 0);
        if (tl)
            path.rQuadTo(-rx, 0, -rx, ry); //top-left corner
        else {
            path.rLineTo(-rx, 0);
            path.rLineTo(0, ry);
        }
        path.rLineTo(0, heightMinusCorners);

        if (bl)
            path.rQuadTo(0, ry, rx, ry);//bottom-left corner
        else {
            path.rLineTo(0, ry);
            path.rLineTo(rx, 0);
        }

        path.rLineTo(widthMinusCorners, 0);
        if (br)
            path.rQuadTo(rx, 0, rx, -ry); //bottom-right corner
        else {
            path.rLineTo(rx, 0);
            path.rLineTo(0, -ry);
        }

        path.rLineTo(0, -heightMinusCorners);

        path.close();//Given close, last lineto can be removed.

        return path;
    }

    private static float widthMinusCorners(float width, float rx) {
        return width - cornerLength(rx);
    }

    private static float heightMinusCorners(float height, float ry) {
        return height - cornerLength(ry);
    }

    private static float cornerLength(float radius) {
        return 2 * radius;
    }

    private static float getTextHeight(TextPaint textPaint) {
        Paint.FontMetrics fm = textPaint.getFontMetrics();
        return Math.abs(fm.ascent) + Math.abs(fm.descent);
    }

}
