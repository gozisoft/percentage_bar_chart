package com.gozisoft.percentagebarchart.sample.ui;

import android.animation.ObjectAnimator;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gozisoft.percentagebarchart.HorizontalBar;
import com.gozisoft.percentagebarchart.PercentageBarChartLayout;
import com.gozisoft.percentagebarchart.sample.R;
import com.gozisoft.percentagebarchart.sample.model.SampleModel;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by rgozim on 22/01/2017.
 */
public class Page1Adapter extends RecyclerView.Adapter<Page1Adapter.ViewHolder> {

    private float mMinValue;
    private float mMaxValue;

    private List<SampleModel> mEntries;
    private Set<Integer> mAnimated;

    public Page1Adapter(List<SampleModel> data) {
        mEntries = data;
        mAnimated = new HashSet<>(data.size());

        calculateMinAndMaxVals(data);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewGroup root = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row_bar_chart, parent, false);

        PercentageBarChartLayout barChartLayout = (PercentageBarChartLayout)
                root.findViewById(R.id.barChart);

        int textColor = parent.getResources().getColor(android.R.color.white);

        barChartLayout.addEntry(new HorizontalBar.Entry()
                .setDrawable(R.drawable.blue_left_to_right_rect)
                .setTextSize(18.0f)
                .setTextColor(textColor));

        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SampleModel entry = mEntries.get(position);

        // Set the label of the current list item
        final String label = entry.getLabel();
        if (!TextUtils.isEmpty(label) && holder.mLabel != null) {
            holder.mLabel.setText(label);
        }

        float value = normalise(entry.getValue(), mMinValue, mMaxValue);
        if (!mAnimated.contains(position)) {
            mAnimated.add(position);

            // This view hasn't been displayed before, animate the setting of percentage
            ObjectAnimator anim = ObjectAnimator.ofFloat(holder.mItem,
                    "percentage", 0, value);
            anim.setDuration(1000);
            anim.start();
        } else {
            holder.mItem.setPercentage(value);
        }
        holder.mItem.setText(String.valueOf(value));
        holder.mItem.setTextColorRes(R.color.md_grey_800);
    }

    @Override
    public int getItemCount() {
        return mEntries.size();
    }


    public void setEntries(List<SampleModel> entries) {
        mEntries = entries;
        mAnimated = new HashSet<>(mEntries.size());

        calculateMinAndMaxVals(entries);
        notifyDataSetChanged();
    }

    private void calculateMinAndMaxVals(List<SampleModel> entries) {
        for (SampleModel model : entries) {
            mMinValue = Math.min(mMinValue, model.getValue());
            mMaxValue = Math.max(mMaxValue, model.getValue());
        }
    }

    /**
     * Function to normalise a value between 0 and 1
     *
     * @param value
     * @param min
     * @param max
     * @return
     */
    static float normalise(float value, float min, float max) {
        // Equation used is:
        // d = input value
        // n = range value
        // v = [(x - min(d)) * (max(n) - min(n)) / max(d) - min(d)] + min(n)
        return (value - min) / max - min;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mLabel;
        PercentageBarChartLayout mBarChart;
        HorizontalBar mItem;

        public ViewHolder(View itemView) {
            super(itemView);
            mLabel = (TextView) itemView.findViewById(R.id.label);
            mBarChart = (PercentageBarChartLayout)
                    itemView.findViewById(R.id.barChart);
            mItem = (HorizontalBar) mBarChart.getChildAt(0);
        }
    }

}
