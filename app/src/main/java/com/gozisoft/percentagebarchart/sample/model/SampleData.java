package com.gozisoft.percentagebarchart.sample.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Riad on 23/01/2017.
 */
public class SampleData {

    public static final List<SampleModel> SAMPLE_DATA = new ArrayList<>();

    static {
        SAMPLE_DATA.add(new SampleModel("vulputate", 30.0f));
        SAMPLE_DATA.add(new SampleModel("nisi", 40.0f));
        SAMPLE_DATA.add(new SampleModel("a", 30.0f));
        SAMPLE_DATA.add(new SampleModel("vulputate", 2.0f));
        SAMPLE_DATA.add(new SampleModel("amet", 200.0f));
        SAMPLE_DATA.add(new SampleModel("neque", 50.0f));
        SAMPLE_DATA.add(new SampleModel("dolor,", 122.0f));
        SAMPLE_DATA.add(new SampleModel("lectus", 100.0f));
        SAMPLE_DATA.add(new SampleModel("semper", 100.0f));
        SAMPLE_DATA.add(new SampleModel("lorem,", 12.0f));
        SAMPLE_DATA.add(new SampleModel("Cras", 40.0f));
        SAMPLE_DATA.add(new SampleModel("metus.", 140.0f));
        SAMPLE_DATA.add(new SampleModel("et", 180.0f));
        SAMPLE_DATA.add(new SampleModel("malesuada", 30.0f));
        SAMPLE_DATA.add(new SampleModel("vitae,", 12.0f));
        SAMPLE_DATA.add(new SampleModel("pharetra", 80.0f));
    }

}
