package com.gozisoft.percentagebarchart.sample.model;

/**
 * Created by Riad on 25/01/2017.
 */

public class SampleModel {
    private String mLabel;
    private float mValue;

    public SampleModel(String label, float value) {
        mLabel = label;
        mValue = value;
    }

    public String getLabel() {
        return mLabel;
    }

    public float getValue() {
        return mValue;
    }
}
