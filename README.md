## Percentage Bar Graph

A horizontal bar graph library that allows for placement of text within a horizontal bar chart.

* Capable of multiple entries to create a segmented bar graph
* Uses percentage based values to determine the width of bar entries
* Text that does not fit within a bar can sit at the end of graph

A sample apk is availbale in [Downloads](https://bitbucket.org/gozisoft/percentage_bar_chart/downloads/) 

### Note
The bar chart uses [GradientDrawable](https://developer.android.com/reference/android/graphics/drawable/GradientDrawable.html) 
in order to draw.

See examples below on how to apply a gradient drawable

### GradientDrawable
```xml
<shape xmlns:android="http://schemas.android.com/apk/res/android"
    android:shape="rectangle">
    <solid android:color="@color/md_blue_300" />
    <corners
        android:bottomRightRadius="6dp"
        android:topRightRadius="6dp" />
</shape>
```

Usage
------

Population of the graph is done through using HorizontalBar.Entry instances. You create entries in 
the form of a builder pattern and pass each instance into PercentageBarChartLayout.addEntry(entry).
For example:

```java
PercentageBarChartLayout barChartLayout = (PercentageBarChartLayout)
        .findViewById(R.id.barChart);

int textColor = parent.getResources().getColor(android.R.color.white);

// Using the Entry builder, easily create and add an entry to
// the layout.
barChartLayout.addEntry(new HorizontalBar.Entry()
        .setDrawable(R.drawable.blue_left_to_right_rect)
        .setPercent
        .setTextSize(18.0f)
        .setTextColor(textColor));
```

You can add entries to your PercentageBarChartLayout in your layout through the use of HorizontalBar. 
An example usage is like so:

```xml
<com.gozisoft.percentagebarchart.PercentageBarChartLayout
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:id="@+id/barChart"
    android:layout_width="match_parent"
    android:layout_height="64dp"
    app:gravity="start">

    <com.gozisoft.percentagebarchart.HorizontalBar
        app:text="25%"
        app:textSize="18sp"
        app:layout_widthPercent="25%"
        app:shapeDrawable="@drawable/blue_left_to_right_rect" />

    <com.gozisoft.percentagebarchart.HorizontalBar
        app:text="50%"
        app:textSize="18sp"
        app:layout_widthPercent="50%"
        app:shapeDrawable="@drawable/orange_left_to_right_rect" />

</com.gozisoft.percentagebarchart.PercentageBarChartLayout>
```

##### HorizontalBar Attributes

| Attribute     | Description |
| ------------- | ------------------------------------------ |
| gravity       | supported gravity left and right for text |
| text          | text to display |  
| textSize      | size of text |
| textColor     | colour of text |
| shapeDrawable | GradientDrawable |

##### PercentBarChartLayout Attributes

| Attribute     | Description |
| ------------- | ---------------------------------------- |
| gravity       | supports left to right and right to left |

##### Environment flags for CI

For Bintray upload set:
`BINTRAY_USER=username`
`BINTRAY_API_KEY=apikey`



```text
Copyright 2017 Riad Gozim

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```